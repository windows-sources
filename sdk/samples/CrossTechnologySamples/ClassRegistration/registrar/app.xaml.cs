using System;
using System.Windows;
using System.Data;
using System.Xml;
using System.Configuration;

namespace Microsoft.Samples.ClassRegistration
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>

    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();
        }

    }
}