#include <winuser.h>
#include "resources.h"
// English (U.S.) resources

#if !defined(AFX_RESOURCE_DLL) || defined(AFX_TARG_ENU)
#ifdef _WIN32
LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
#pragma code_page(1252)
#endif //_WIN32

#define IDM_NO_OP		   999  // used for disabled menu items
#define IDM_CRASH_APP      400


/////////////////////////////////////////////////////////////////////////////
//
// Icon
//

// Icon with lowest ID value placed first to ensure application icon
// remains consistent on all systems.
SAFEPAD                 ICON                    "Notepad Icon.ICO"

/////////////////////////////////////////////////////////////////////////////
//
// Menu
//

SAFEPAD MENUEX 
BEGIN
    POPUP "&File",                          65535,MFT_STRING,MFS_ENABLED
    BEGIN
        MENUITEM "&New\tCtrl+N",                IDM_NEW,MFT_STRING,MFS_ENABLED
        MENUITEM "&Open...\tCtrl+O",            IDM_OPEN,MFT_STRING,MFS_ENABLED
        MENUITEM "&Save\tCtrl+S",               IDM_SAVE,MFT_STRING,MFS_ENABLED
        MENUITEM "Save &As...",                 IDM_SAVEAS,MFT_STRING,MFS_GRAYED
        MENUITEM MFT_SEPARATOR
        MENUITEM "Page Set&up...",              IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM "&Print...\tCtrl-P",           IDM_NO_OP,MFT_STRING,MFS_GRAYED
        MENUITEM MFT_SEPARATOR
        MENUITEM "E&xit",                       IDM_EXIT,MFT_STRING,MFS_ENABLED
    END
    POPUP "&Edit",                          65535,MFT_STRING,MFS_ENABLED
    BEGIN
        MENUITEM "&Undo\tCtrl+Z",               IDM_UNDO,MFT_STRING,MFS_GRAYED
        MENUITEM MFT_SEPARATOR
        MENUITEM "Cu&t\tCtrl+X",                IDM_NO_OP,MFT_STRING,MFS_GRAYED
        MENUITEM "&Copy\tCtrl+C",               IDM_NO_OP,MFT_STRING,MFS_GRAYED
        MENUITEM "&Paste\tCtrl+V",              IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM "Delete\tDel",					IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM MFT_SEPARATOR        
		MENUITEM "Find\tCtrl+F",				IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM "Find Next\tF3",				IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM "Replace...\tCtrl+H",			IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM "Go To...\tCtrl+G",			IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM MFT_SEPARATOR        
		MENUITEM "Select All\tCtrl+A",			IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM "Time/Date\tF5",				IDM_NO_OP,MFT_STRING,MFS_GRAYED
    END
    POPUP "F&ormat",						65535,MFT_STRING,MFS_ENABLED
    BEGIN
        MENUITEM "Word Wrap",					IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM "Font...",						IDM_NO_OP,MFT_STRING,MFS_GRAYED
	END
	POPUP "&Help",							65535,MFT_STRING,MFS_ENABLED
    BEGIN
		MENUITEM "Help Topics",					IDM_NO_OP,MFT_STRING,MFS_GRAYED
		MENUITEM MFT_SEPARATOR
        MENUITEM "&About SafePad...",           IDM_NO_OP,MFT_STRING,MFS_GRAYED
    END
    POPUP "&Special",                       65535,MFT_STRING,MFS_ENABLED
    BEGIN
        MENUITEM "&Crash Application",          IDM_CRASH_APP,MFT_STRING,MFS_GRAYED
        MENUITEM "&Hang Application",           IDM_HANG_APP,MFT_STRING,MFS_GRAYED
        MENUITEM MFT_SEPARATOR
        MENUITEM "Application &Restart",         IDM_ENABLE_RESTART,MFT_STRING,MFS_ENABLED
        MENUITEM "Document Reco&very",           IDM_ENABLE_RECOVERY,MFT_STRING,MFS_ENABLED
        MENUITEM "&Error Reporting",             IDM_ENABLE_REPORTING,MFT_STRING,MFS_ENABLED
    END

END


/////////////////////////////////////////////////////////////////////////////
//
// Accelerator
//

SAFEPAD ACCELERATORS 
BEGIN
    VK_F1,          IDM_HELPCONTENTS,       VIRTKEY 
    "?",            IDM_ABOUT,              ASCII,  ALT
    "/",            IDM_ABOUT,              ASCII,  ALT
END


/////////////////////////////////////////////////////////////////////////////
//
// Version
//

1 VERSIONINFO
 FILEVERSION 3,5,0,0
 PRODUCTVERSION 3,5,0,0
 FILEFLAGSMASK 0x3fL
#ifdef _DEBUG
 FILEFLAGS 0xbL
#else
 FILEFLAGS 0xaL
#endif
 FILEOS 0x10001L
 FILETYPE 0x1L
 FILESUBTYPE 0x0L
BEGIN
    BLOCK "StringFileInfo"
    BEGIN
        BLOCK "040904E4"
        BEGIN
            VALUE "CompanyName", "Microsoft Corporation"
            VALUE "FileDescription", "SafePad"
            VALUE "FileVersion", "0.0"
            VALUE "InternalName", "SafePad"
            VALUE "LegalCopyright", "Copyright (c) Microsoft Corporation."
            VALUE "LegalTrademarks", "Microsoft(R) is a registered trademark of Microsoft Corporation. Windows(TM) is a trademark of Microsoft Corporation"
            VALUE "ProductName", "SafePad"
            VALUE "ProductVersion", "3.6"
        END
        BLOCK "041104E4"
        BEGIN
            VALUE "CompanyName", "Microsoft Corporation"
            VALUE "FileDescription", "SafePad"
            VALUE "FileVersion", "0.0"
            VALUE "InternalName", "SafePad"
            VALUE "LegalCopyright", "Copyright (c) Microsoft Corporation."
            VALUE "LegalTrademarks", "Microsoft(R) is a registered trademark of Microsoft Corporation. Windows(TM) is a trademark of Microsoft Corporation"
            VALUE "ProductName", "SafePad"
            VALUE "ProductVersion", "3.6"
        END
    END
    BLOCK "VarFileInfo"
    BEGIN
        VALUE "Translation", 0x409, 1252, 0x411, 1252
    END
END


#ifdef APSTUDIO_INVOKED
/////////////////////////////////////////////////////////////////////////////
//
// TEXTINCLUDE
//

1 TEXTINCLUDE 
BEGIN
    "r\0"
    "e\0"
    "s\0"
    "o\0"
    "u\0"
    "r\0"
    "c\0"
    "e\0"
    ".\0"
    "h\0"
    "\0"
    "\0"
END

2 TEXTINCLUDE 
BEGIN
    "\0"
    "\0"
END

3 TEXTINCLUDE 
BEGIN
    "\r\0"
    "\n"
    "\0"
    "\0"
    "\0"
END

#endif    // APSTUDIO_INVOKED


/////////////////////////////////////////////////////////////////////////////
//
// String Table
//

STRINGTABLE 
BEGIN
    IDS_APP_TITLE           "Untitled - SafePad"
    IDS_DISPLAYCHANGED      "Display Changed"
    IDS_VER_INFO_LANG       "\\StringFileInfo\\040904E4\\"
    IDS_VERSION_ERROR       "Error %lu"
    IDS_NO_HELP             "Unable to activate help"
END

#endif    // English (U.S.) resources
/////////////////////////////////////////////////////////////////////////////



#ifndef APSTUDIO_INVOKED
/////////////////////////////////////////////////////////////////////////////
//
// Generated from the TEXTINCLUDE 3 resource.
//


/////////////////////////////////////////////////////////////////////////////
#endif    // not APSTUDIO_INVOKED

